Instructions for repeating experiment for Improving Reproducibility of the Distrbuted Computational Experiments.

1. Download benchmarks NPB3.3.1-NPS www.nas.nasa.gov/pubilcations/npb.html
   
2. Follow instructions in README.Install to compile benchmarks

3. Download and install MPICH at https://www.mpich.org/documentation/guides/

4. Download PTU package from https://www.dropbox.com/s/n0mb8o3dq66w8zk/PTU_Quan_4_2018.zip?dl=0

5. Configure three nodes to communicate with one another with SSH.  One will be a master node and the other two will be slave nodes.

6. Create an MPICH configuration file to inform MPICH where the remote nodes are. Each line can be an IP address.
Example:
140.192.249.93
140.192.249.4

7. Run
/Path/To/ptu-audit mpiexec -n 2 -f /Path/To/MPICH_config /Path/To/NPB_Directory/bin/lu-mz.B.2
